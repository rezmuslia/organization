package com.organization.organization.Repository;


import com.organization.organization.Entity.Organization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrganizationRepository extends JpaRepository <Organization, Integer> {
    List<Organization> findAllByName(String name);

    Organization findById(int id);

    @Query(value="update ORGANIZATION set IS_ACTIVE = ?2, NAME = ?3 where id = ?1", nativeQuery = true )
    void up(int id, boolean is, String name);
}
