package com.organization.organization.Controller;


import com.organization.organization.Entity.Organization;
import com.organization.organization.Service.OrganisationService;
import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/organizations")
public class OrganizationController {
    private final OrganisationService organisationService;

    @Autowired
    public OrganizationController(OrganisationService organisationService) {
        this.organisationService = organisationService;
    }

    @GetMapping
    public List<Organization>getAll(){
         return organisationService.findAll();
    }

    @GetMapping(path= "/searchByName", params = "name")
    public List<Organization> searchByName(@RequestParam String name){
          return organisationService.searchByName(name);
    }

    @GetMapping(params = "id")
    public List<Organization> searchById(@RequestParam Integer id){
        return organisationService.searchById(id);
    }


    @PostMapping("/save")
    public void add(@RequestBody Organization organization){
        organisationService.add(organization);

    }

    //@PostMapping("/delete")
    // public Result delete(@RequestBody int id){
      //  organisationService.delete(id);}
    @PostMapping("/update")
    public void update(@RequestBody Organization organization){
        organisationService.update(organization);
    }


}
