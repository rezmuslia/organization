package com.organization.organization.Service;

import com.organization.organization.Entity.Organization;
import com.organization.organization.Repository.OrganizationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrganisationServiceImpl implements OrganisationService {
    private final OrganizationRepository organizationRepository;


    @Autowired
    public OrganisationServiceImpl(OrganizationRepository organizationRepository) {
        this.organizationRepository = organizationRepository;
    }

    @Override
    public List<Organization> findAll() {
        return organizationRepository.findAll();
    }

    @Override
    public void add(Organization organization) {
        organizationRepository.save(organization);
    }

    @Override
    public List<Organization> searchByName(String name) {
        return organizationRepository.findAllByName(name);
    }

    public Organization findOne(Integer id) {
        return organizationRepository.getOne(id);
    }

    @Override
    public void delete(int id) {
        organizationRepository.deleteById(id);
    }


    public void update(Organization organization) {
        organizationRepository.saveAndFlush(organization);
    }

    @Override
    public List<Organization> searchById(Integer id) {
        return null;
    }

    public void up(int id, boolean is, String name) {
        organizationRepository.up(id, is, name);
    }
}
