package com.organization.organization.Service;

import com.organization.organization.Entity.Organization;

import java.util.List;

public interface OrganisationService {
    List<Organization> findAll();

    void add(Organization organization);

    List<Organization> searchByName(String name);

    void delete(int id);

    void update(Organization organization);

    List<Organization> searchById(Integer id);




}
